# Snitch - Monitoring IOT Devices

The `snitch` tool is a Go program for monitoring IOT devices and logging their communication. Currently, it is only capable of monitoring MQTT.

## Installation

1. Make sure Go is installed on your system and is included in the PATH environment variable.
2. Clone the repository with `git clone https://gitlab.com/ludal/snitch.git`
3. Change into the directory with `cd snitch`
4. Install the tool with `go install`

## Usage

To use the `snitch` tool, simply run the command `snitch`. The tool will begin monitoring MQTT and logging all communication.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Contributing

If you would like to improve the `snitch` tool, simply send a pull request with your changes to the GitLab repository. We always welcome contributions from the community!
