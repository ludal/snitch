v=0.10

all: bins

bins:
	GOOS=windows GOARCH=amd64 go build -o snitch-windows-amd64.exe
	GOOS=windows GOARCH=386 go build -o snitch-windows-386.exe
	GOOS=linux GOARCH=386 go build -o snitch-linux-386
	GOOS=linux GOARCH=amd64 go build -o snitch-linux-amd64
	GOOS=linux GOARCH=arm go build -o snitch-linux-arm
	GOOS=linux GOARCH=arm go build -o snitch-linux-arm64
	GOOS=darwin GOARCH=arm64 go build -o snitch-darwin-arm64
	GOOS=darwin GOARCH=amd64 go build -o snitch-darwin-amd64
	zip -m snitch-v$v-multi.zip snitch-*
version:
	echo v$v