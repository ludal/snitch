package main

import (
	"flag"
	"fmt"
	"net/url"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func main() {
	// Set up options
	opts := mqtt.NewClientOptions()
	server := "tcp://localhost:1883"
	if len(os.Args) > 1 {
		serverURL, err := url.Parse(os.Args[1])
		if err != nil {
			fmt.Println("Error parsing server URL:", err)
			os.Exit(1)
		}
		server = serverURL.String()
		if serverURL.Port() == "" {
			if serverURL.Scheme == "mqtt" {
				server += ":1883"
			} else if serverURL.Scheme == "mqtts" {
				server += ":8883"
			}
		}
	}
	opts.AddBroker(server)
	opts.SetClientID("snitch")

	// Parse command-line arguments for optional username and password
	filename := flag.String("filename", "snitchLog.txt", "fiel to store")
	username := flag.String("username", "", "MQTT username")
	password := flag.String("password", "", "MQTT password")
	help := flag.Bool("help", false, "Show usage message")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		fmt.Fprintf(os.Stderr, " snitch [options] [server URL]\n\n")
		fmt.Fprintf(os.Stderr, "Options:\n")
		flag.PrintDefaults()
	}
	flag.Parse()

	// Show usage message and exit if the help flag is set
	if *help {
		flag.Usage()
		os.Exit(0)
	}

	// Set username and password if provided
	if *username != "" {
		opts.SetUsername(*username)
	}
	if *password != "" {
		opts.SetPassword(*password)
	}

	// Create a new client
	client := mqtt.NewClient(opts)

	// Connect to the MQTT broker
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	// Subscribe to all topics
	if token := client.Subscribe("#", 0, func(client mqtt.Client, message mqtt.Message) {
		output := fmt.Sprintf("[%s-%s] %s\n", time.Now().Format(time.RFC3339), message.Topic(), message.Payload())
		fmt.Print(output)
		writeToFile(output, *filename)
	}); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	// Sleep forever
	select {}
}

const maxFileSice = 1 * 1024 * 1024 // 1MB

func ensureFile(filename string) os.File {

	file, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		println("not exist ", os.IsNotExist(err))
		file, err = os.Create(filename)
	}

	// Check the size of the file
	fileInfo, err := file.Stat()
	if err != nil {
		fmt.Println("Error getting file info:", err)
		os.Exit(1)
	}
	if fileInfo.Size() > maxFileSice {
		// Create a new file with an incremented index in the name
		index := 1
		for {
			newFilename := fmt.Sprintf("%s%d%s", filename[:len(filename)-4], index, filename[len(filename)-4:])
			_, err := os.Stat(newFilename)
			if os.IsNotExist(err) {
				file, err = os.Create(newFilename)
				if err != nil {
					fmt.Println("Error creating file:", err)
					os.Exit(1)
				}
				break
			}
			index++
		}

	}

	return *file
}

func writeToFile(content string, fileName string) error {
	file := ensureFile(fileName)
	_, err := file.WriteString(content)
	if err != nil {
		return err
	}
	return nil
}
